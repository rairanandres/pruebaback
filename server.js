const express = require("express");
const { ApolloServer } = require("apollo-server-express");
const { typeDefs } = require("./schema.js");
const { resolvers } = require("./resolvers.js");
const path = require("path");

const app = express();
const server = new ApolloServer({ typeDefs, resolvers });

server.applyMiddleware({ app });

// -----> para produccion < ------
// app.use(express.static("public"));
// app.get("*", (req, res) => {
//   res.sendFile(path.resolve(__dirname, "public", "index.html"));
// });
// -------------------------------

const PORT = process.env.PORT || 5000;

app.listen(PORT, () =>
  console.log(`Servidor Funcionando !!! en el puerto: ${PORT}`)
);
