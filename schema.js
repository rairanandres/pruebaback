const { gql } = require("apollo-server-express");

const typeDefs = gql`
  type Registro {
    id: ID
    nombreColumna: [String]
    datos: [String]
  }
  type Query {
    mostrarRegistros: [Registro]
  }
  type Mutation {
    nuevoRegistro(input: InputRegistro): Registro
  }
  input InputRegistro {
    id: ID
    nombreColumna: [String]
    datos: [String]
  }
`;

exports.typeDefs = typeDefs;
