const mongoose = require("mongoose");

mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost/importador", { useNewUrlParser: true });

const RegistroSchema = new mongoose.Schema({
  nombreColumna: Array,
  datos: Array
});

const Registro = mongoose.model("Registro", RegistroSchema);

exports.Registro = Registro;
