const { Registro } = require("./db");

const resolvers = {
  Query: {
    mostrarRegistros: root => {
      return Registro.find();
    }
  },

  Mutation: {
    nuevoRegistro: (root, { input }) => {
      const nuevaFila = new Registro({
        nombreColumna: input.nombreColumna,
        datos: input.datos
      });
      //MongoDb crea el ID que se le asigna al objeto
      nuevaFila.id = nuevaFila._id;

      return new Promise((resolve, object) => {
        nuevaFila.save(error => {
          if (error) rejects(error);
          else resolve(nuevaFila);
        });
      });
    }
  }
};

exports.resolvers = resolvers;
