import React from "react";
import styled from "styled-components";
import { Query } from "react-apollo";
import { MOSTRAR_REGISTROS } from "./queries";

const ContentTable = styled.div`
  width: 100%;
  overflow-x: scroll;
  ::-webkit-scrollbar {
    background: white;
    border-radius: 10px;
    width: 8px;
    height: 8px;
  }
  ::-webkit-scrollbar-thumb {
    background: grey;
    border-radius: 10px;
  }
  ::-webkit-scrollbar-button {
    background: black;
    border-radius: 10px;
  }
`;
const Table = styled.table`
  /* border-spacing: 15px; */
  border-collapse: collapse;
  border: 1px solid #bdbdbd;
  text-align: center;
  white-space: nowrap;
  width: 100%;
  border-radius: 10px;
  background: white;
`;

const TableHead = styled.th`
  border: 1px solid #bdbdbd;
  background: #ffffff;
  padding: 5px;
  color: black;
  background: white;
  border-radius: 4px;
  font-weight: 500;
`;
const TableRow = styled.tr`
  :nth-child(even) {
    background: #fafafa;
  }
`;
const TableField = styled.td`
  border: 1px solid #bdbdbd;
  padding: 8px 5px 8px 5px;
  color: black;
  font-weight: 550;
`;

const Tabla = () => {
  let documents = [
    {
      nombreColumna: ["nombres", "cedula", "telefono"],
      datos: ["andres", "1032434248", "3194807244"]
    },
    { datos: ["andres", "1032434248", "3194807244"] }
  ];
  return (
    <ContentTable>
      <Query query={MOSTRAR_REGISTROS} pollInterval={500}>
        {({ loading, error, data, startPolling, stopPolling }) => {
          if (loading) return "Cargando...";
          if (error) return `Error: ${error.message}`;
          console.log(data.mostrarRegistros);
          let reemplazo = [];
          let sinDatosCabeza =
            data.mostrarRegistros.length > 0
              ? reemplazo.push(data.mostrarRegistros[0].nombreColumna)
              : reemplazo.push(["sín Datos"]);
          console.log(reemplazo);
          return (
            <Table>
              <thead>
                <TableRow>
                  {reemplazo[0].map(titulo => {
                    console.log(titulo);
                    return <TableHead>{titulo}</TableHead>;
                  })}
                </TableRow>
              </thead>
              <tbody>
                {data.mostrarRegistros.map(doc => (
                  <TableRow>
                    {doc.datos.map(datico => (
                      <TableField>{datico}</TableField>
                    ))}
                  </TableRow>
                ))}
              </tbody>
            </Table>
          );
        }}
      </Query>
    </ContentTable>
  );
};

export default Tabla;
