import React, { useState } from "react";
import styled from "styled-components";
import xlsxParser from "xlsx-parse-json";
import { CREAR_REGISTRO } from "./mutations";
import { Mutation } from "react-apollo";
import { withRouter } from "react-router-dom";

const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 50%;
  height: 60%;
  margin: auto;
  background: #fafafa;
  border-radius: 8px;
`;
const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 10px;
  width: 100%;
`;
const Label = styled.label`
  margin: 10px;
  font-style: italic;
`;

const ImportarColumnas = props => {
  const [data, setData] = useState([]);
  const [name, setName] = useState("");

  let listaValores = [];
  let listaEncabezados = [];

  const leerArchivo = e => {
    const archivo = e.target.files[0];
    setName(e.target.files[0]);
    xlsxParser.onFileSelection(archivo).then(file => {
      console.log(file);
      setData(file.prueba);
    });
  };

  data.map(obj => {
    listaValores.push(Object.values(obj));
    listaEncabezados.push(Object.keys(obj));
  });
  console.log(listaValores);
  console.log(listaEncabezados);

  return (
    <Content>
      <input
        type="file"
        id="input-file"
        onChange={leerArchivo}
        accept=".xlsx"
      />
      <Label>{name.name}</Label>
      <Mutation
        mutation={CREAR_REGISTRO}
        onCompleted={() => props.history.push("/")}
      >
        {crearRegistro => (
          <Form
            onSubmit={e => {
              e.preventDefault();
              listaValores.map((valor, index) => {
                const input = {
                  nombreColumna: listaEncabezados[0],
                  datos: valor
                };
                crearRegistro({
                  variables: { input }
                });
              });
              //}
              alert("ARCHIVO SUBIDO CON ÉXITO");
              //window.location.reload();
            }}
          >
            <button type="submit" name="subir archivo">
              Subir Archivo
            </button>
          </Form>
        )}
      </Mutation>
    </Content>
  );
};

export default ImportarColumnas;
