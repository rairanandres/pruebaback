import gql from "graphql-tag";

export const MOSTRAR_REGISTROS = gql`
  query verRegistros {
    mostrarRegistros {
      datos
      nombreColumna
    }
  }
`;
