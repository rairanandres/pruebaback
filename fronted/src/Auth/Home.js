import React, { useState } from "react";
import styled from "styled-components";
import Registro from "./Registro";
import Login from "./Login";

const Container = styled.div`
  background: #fafafa;
  height: 100vh;
`;

const ContentBotones = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const Button = styled.button`
  font-size: calc(0.7vw + 0.5rem);
  font-family: "roboto", sans-serif;
  text-decoration: none;
  font-weight: bold;
  padding: 10px 20px 10px 20px;
  margin: 5px;
  border-radius: 5px;
  border: 1.3px solid #212121;
  box-sizing: border-box;
  background: #212121;
  color: whitesmoke;
  outline: none;
  cursor: pointer;
  :hover {
    background: #1b1b1b;
    color: white;
  }
  @media (min-width: 280px) and (max-width: 425px) {
    font-size: calc(1vh + 0.3rem);
    margin: 6px 0 0 0;
  }
  @media (min-width: 426px) and (max-width: 800px) {
    font-size: calc(1vw + 0.7rem);
  }
`;

const Home = () => {
  const [login, setLogin] = useState(false);
  const [registro, setRegistro] = useState(false);

  const MostrarLogin = () => {
    setLogin(!login);
  };
  const MostrarRegistro = () => {
    setRegistro(!registro);
  };
  return (
    <Container>
      <ContentBotones>
        <Button type="button" onClick={MostrarLogin}>
          Entrar
        </Button>
        <Login stateFalse={login} stateTrue={MostrarLogin} />
        <Button type="button" onClick={MostrarRegistro}>
          Registrarse
        </Button>
        <Registro stateFalse={registro} stateTrue={MostrarRegistro} />
      </ContentBotones>
    </Container>
  );
};

export default Home;
