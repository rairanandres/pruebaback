import React, { useState } from "react";
import styled from "styled-components";

import Modal from "../Common/Modal";

const Modaloverlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1040;
  width: 100vw;
  height: 100vh;
  background-color: #000;
  opacity: 0.5;
`;
const ModalWrapper = styled.div`
  position: fixed;
  top: 12%;
  left: 30%;
  right: 30%;
  z-index: 1050;
  height: 100%;
  overflow-x: hidden;
  overflow-y: auto;
  outline: 0;
  border-radius: 10px;
`;
const ModalStyled = styled.div`
  z-index: 100;
  background: white;
  position: relative;
  margin: 1.75rem auto;
  border-radius: 3px;
  max-width: 1000px;
  padding: 2rem;
  border-radius: 10px;
`;
const ModalHeader = styled.div`
  display: flex;
  justify-content: flex-end;
`;
const Img = styled.img`
  height: 35px;
  width: 35px;
  cursor: pointer;
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  width: 90%;
  height: 45%;
`;

const GroupInput = styled.div`
  display: flex;
  flex-direction: column;
  width: 80%;
`;

const Label = styled.label`
  font-size: calc(0.8vw + 0.625rem);
  font-weight: 700;
  color: #3c3c3c;
  margin: 0 0 0.25rem 0;
`;

const Input = styled.input`
  font-size: calc(1.1vw + 0.625rem);
  padding: 8px 6px 8px 6px;
  border: 1.3px solid #bdbdbd;
  box-sizing: border-box;
  border-radius: 5px;
  outline-color: #6d0259;
`;
const Button = styled.button`
  font-size: calc(1vw + 0.2rem);
  font-family: "roboto", sans-serif;
  padding: 7px 25px 7px 25px;
  font-weight: 500;
  background: #6d0259;
  color: #ffffff;
  outline: none;
  cursor: pointer;
  margin: 5px 20px 10px 20px;
  border-radius: 0.25em;
  border: 2px solid #6d0259;
  box-sizing: border-box;
  :disabled {
    background: #ccc;
    color: #fff;
    border: 2px solid #ccc;
  }
`;

const Login = props => {
  // const validarBoton = () => {
  //   const noValido =
  //   return noValido;
  // };
  return props.stateFalse ? (
    <Modal>
      <Modaloverlay />
      <ModalWrapper>
        <ModalStyled>
          <ModalHeader></ModalHeader>
          <Form>
            <GroupInput>
              <Label>Usuario</Label>
              <Input type="text" />
            </GroupInput>
            <GroupInput>
              <Label>Contraseña</Label>
              <Input type="text" />
            </GroupInput>
            <Button
              type="submit"
              //  disabled={validarBoton()}
            >
              Entrar
            </Button>
          </Form>
        </ModalStyled>
      </ModalWrapper>
    </Modal>
  ) : null;
};

export default Login;
