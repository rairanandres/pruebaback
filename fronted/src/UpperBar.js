import React, { useContext } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { DashContext } from "./contexts/dashboard";
import { withRouter } from "react-router-dom";

const Container = styled.div`
  grid-column: 1 / 13;
  grid-row: 1 / 1;
  display: grid;
  grid-template-columns: repeat(8, 1fr);
  align-items: center;
  justify-content: center;
  height: 60px;
  padding: 0 8px 0 8px;
  background-color: #212121;
  box-shadow: 0px 2px 5px 1px rgba(0, 0, 0, 0.23);
  @media (min-width: 320px) and (max-width: 425px) {
    display: flex;
    justify-content: space-between;
    grid-column: 1 / 5;
  }
  @media (min-width: 426px) and (max-width: 800px) {
    grid-column: 1 / 9;
  }
`;

const TitleApp = styled(Link)`
  font-size: calc(1vw + 0.3rem);
  grid-column: 1 / 2;
  font-weight: bold;
  text-decoration: none;
  color: white;
  @media (min-width: 320px) and (max-width: 425px) {
    font-size: calc(1vh + 0.7rem);
  }
  @media (min-width: 426px) and (max-width: 768px) {
    font-size: calc(1vw + 0.8rem);
  }
`;

const BoxItems = styled.div`
  grid-column: 7 / 9;
  display: flex;
  align-items: center;
  justify-content: space-around;
  @media (min-width: 320px) and (max-width: 425px) {
    display: none;
  }
  @media (min-width: 426px) and (max-width: 768px) {
    display: none;
  }
`;
const LinkItem = styled(Link)`
  font-size: calc(1vw + 0.4rem);
  color: whitesmoke;
  text-decoration: none;
  margin: 0 15px 0 15px;
  :hover {
    font-weight: bold;
    color: white;
    text-decoration: underline;
  }
`;
const LinkItemRuta = styled(LinkItem)`
  text-decoration: underline;
`;

const BoxUser = styled.div`
  display: flex;
  justify-self: end;
  align-items: center;
  @media (min-width: 320px) and (max-width: 425px) {
    display: none;
  }
  @media (min-width: 426px) and (max-width: 768px) {
    display: none;
  }
`;

const ImgMenuPhone = styled.img`
  display: none;
  cursor: pointer;
  @media (min-width: 320px) and (max-width: 425px) {
    display: flex;
    margin-right: 4px;
  }
  @media (min-width: 426px) and (max-width: 768px) {
    display: flex;
  }
`;
const BoxMenuPhone = styled.div`
  background: white;
  border: 1px solid #bdbdbd;
  flex-direction: column;
  align-items: flex-start;
  margin: 4px 4px 0 0;
  padding: 4px 10px 4px 10px;
  border-radius: 4px;
  z-index: 100;
  @media (min-width: 220px) and (max-width: 425px) {
    grid-column: 2 / 5;
    grid-row: 2 / 4;
    display: flex;
  }
  @media (min-width: 426px) and (max-width: 768px) {
    grid-column: 5 / 9;
    grid-row: 2 / 5;
    display: flex;
  }
`;

const LinkPhone = styled(Link)`
  font-size: calc(1vh + 1rem);
  color: #3c3c3c;
  text-decoration: none;
  margin: 4px 0 4px 0;
  width: 100%;
  white-space: nowrap;
  :hover {
    color: #000;
    text-decoration: underline;
  }
`;
const Line = styled.hr`
  color: #dbdbdb;
  margin: 10px 0px 2px 0;
  width: 100%;
`;
const BoxExit = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  width: 97%;
  padding: 4px;
  border-radius: 0px 0px 10px 10px;
  cursor: pointer;
  :hover {
    text-decoration: none;
    background: #dbdbdb;
  }
`;
const ImgExit = styled.img`
  height: 17px;
  width: 17px;
`;
const Exit = styled.p`
  font-size: calc(1vh + 0.6rem);
  margin: 0 0 0 10px;
`;

const UpperBar = props => {
  const [state, dispatch] = useContext(DashContext);

  //efecto de subrayado en el link cuando me encuentre en esa ruta
  let Importar =
    props.location.pathname === "/importar" ? (
      <LinkItemRuta to={"/importar"}>Importar</LinkItemRuta>
    ) : (
      <LinkItem to={"/importar"}>Importar</LinkItem>
    );
  let Registro =
    props.location.pathname === "/registro" ? (
      <LinkItemRuta to={"/registro"}>Registrarse</LinkItemRuta>
    ) : (
      <LinkItem to={"/registro"}>Registrarse</LinkItem>
    );

  return (
    <>
      <Container>
        <TitleApp
          to={"/"}
          onClick={() => {
            dispatch({
              payload: state.initialState
            });
          }}
        >
          Prueba Back
        </TitleApp>
        <BoxItems>
          {Importar}
          {Registro}
          <BoxUser></BoxUser>
        </BoxItems>
      </Container>
      {state.menuphone ? (
        <BoxMenuPhone>
          <LinkPhone to={"/importar"}>Importar</LinkPhone>
          <LinkPhone to={"/registro"}>Registrarse</LinkPhone>
          <Line />
          <BoxExit>
            <Exit>Cerrar sesión</Exit>
          </BoxExit>
        </BoxMenuPhone>
      ) : null}
    </>
  );
};

export default withRouter(UpperBar);
