import gql from "graphql-tag";

export const CREAR_REGISTRO = gql`
  mutation crearTrabajador($input: InputRegistro) {
    nuevoRegistro(input: $input) {
      nombreColumna
      datos
    }
  }
`;
