import React, { useContext } from "react";
import { ApolloProvider } from "react-apollo";
import ApolloClient, { InMemoryCache } from "apollo-boost";
import { BrowserRouter as Router } from "react-router-dom";
import DashProvider from "./contexts/dashboard";
import Dashboard from "./Dashboard";
import { AuthContext } from "./contexts/auth";
import Home from "./Auth/Home";

const conexionServer = new ApolloClient({
  uri: "http://localhost:5000/graphql",
  cache: new InMemoryCache({
    addTypename: false
  }),
  onError: ({ networkError, graphQLErrors }) => {
    console.log("graphQLErrors", graphQLErrors);
    console.log("networkErrors", networkError);
  }
});

const App = () => {
  const [state, dispatch] = useContext(AuthContext);
  return (
    <ApolloProvider client={conexionServer}>
      <Router>
        {state.auth === true ? (
          <DashProvider>
            <Home />
          </DashProvider>
        ) : (
          <DashProvider>
            <Dashboard />
          </DashProvider>
        )}
      </Router>
    </ApolloProvider>
  );
};

export default App;
