import React, { useReducer, createContext } from "react";
export const DashContext = createContext({});
let initialState = {
  settings: false,
  menuphone: false,
  cardOne: false,
  cardTwo: false,
  graficaDiaVenta: false,
  graficaMesVenta: false
};
let dashReducer = (state, action) => {
  switch (action.type) {
    case "OPEN_CLOSE_SETTINGS":
      return { ...state, settings: action.payload };
    case "OPEN_CLOSE_MENUPHONE":
      return { ...state, menuphone: action.payload };
    case "OPEN_CLOSE_CARDONE":
      return { ...state, cardOne: action.payload };
    case "OPEN_CLOSE_CARDTWO":
      return { ...state, cardTwo: action.payload };
    case "OPEN_CLOSE_GRAFICADIAVENTA":
      return { ...state, graficaDiaVenta: action.payload };
    case "OPEN_CLOSE_GRAFICAMESVENTA":
      return { ...state, graficaMesVenta: action.payload };
    default:
      return initialState;
  }
};
const DashProvider = ({ children }) => {
  const [state, dispatch] = useReducer(dashReducer, initialState);

  return (
    <DashContext.Provider value={[state, dispatch]}>
      {children}
    </DashContext.Provider>
  );
};

export default DashProvider;
