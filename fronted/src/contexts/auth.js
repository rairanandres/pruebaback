import React, { useReducer, createContext } from "react";
export const AuthContext = createContext({});

let initialState = {
  auth: false,
  rol: "",
  user: {},
  error: false
};

let authReducer = (state, action) => {
  switch (action.type) {
    case "AUTH":
      return { ...state, auth: action.payload };
    case "ROL":
      return { ...state, rol: action.payload };
    case "USER":
      return { ...state, user: action.payload };
    case "ERROR":
      return { ...state, error: action.payload };
    default:
      return initialState;
  }
};

const AuthProvider = ({ children }) => {
  const [state, authdispatch] = useReducer(authReducer, initialState);

  return (
    <AuthContext.Provider value={[state, authdispatch]}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthProvider;
