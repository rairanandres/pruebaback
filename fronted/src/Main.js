import React from "react";
import styled from "styled-components";
import Tabla from "./Tabla";

const Container = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 40px;
  @media (min-width: 220px) and (max-width: 425px) {
    grid-template-columns: repeat(1, 1fr);
    grid-gap: 10px;
  }
`;

const Main = () => {
  return (
    <Container>
      <Tabla />
    </Container>
  );
};
export default Main;
