import React, { useContext } from "react";
import { Route, Switch } from "react-router-dom";
import styled from "styled-components";
import { DashContext } from "./contexts/dashboard";
import UpperBar from "./UpperBar";
import Main from "./Main";
import Registro from "./Auth/Registro";
import ImportarColumnas from "./ImportarColumnas";

const Container = styled.div`
  display: grid;
  grid-template-columns: repeat(12, 1fr);
  grid-template-rows: repeat(10, 1fr);
  height: 100vh;
  @media (min-width: 426px) and (max-width: 768px) {
    display: grid;
    grid-template-columns: repeat(8, 1fr);
    height: 100vh;
  }
  @media (min-width: 220px) and (max-width: 425px) {
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    height: 100vh;
  }
`;
const Content = styled.div`
  grid-column: 1 / 13;
  grid-row: 2 / 11;
  padding: 10px;
  /* height: 100%; */
  scroll-behavior: smooth;
  overflow-y: auto;
  @media (min-width: 220px) and (max-width: 425px) {
    grid-column: 1 / 5;
    padding: 4px;
  }
  @media (min-width: 426px) and (max-width: 768px) {
    grid-column: 1 / 9;
    padding: 8px;
  }
`;

const Dashboard = () => {
  const [state, dispatch] = useContext(DashContext);
  return (
    <Container>
      <UpperBar />
      <Content
        onClick={() => {
          dispatch({
            type: "OPEN_CLOSE_MENUPHONE",
            payload: state.menuphone === true ? !state.menuphone : null
          });
        }}
      >
        <Switch>
          <Route exact path="/importar" component={ImportarColumnas} />
          <Route exact path="/registro" component={Registro} />
          <Route exact path="/" component={Main} />
        </Switch>
      </Content>
    </Container>
  );
};

export default Dashboard;
